CC=g++
CFLAGS=-c -Wall -Isrc -Isrc/3rdparty -DUSING_WINDOWS -O3
LDFLAGS=
OBJDIR=obj
SOURCES=src/main.cpp src/util/directories.cpp src/util/image.cpp src/util/progressbar.cpp src/3rdparty/lodepng/lodepng.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=pngtiler
PREFIX=/usr/local

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@

.cpp.o:
	$(CC) $< -o $@ $(CFLAGS)

clean:
	rm *.o pngtiler

install: $(EXECUTABLE)
	install -m 0755 $(EXECUTABLE) $(PREFIX)/bin

.PHONY: install
