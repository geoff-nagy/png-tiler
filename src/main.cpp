// PNGTiler
// Geoff Nagy
// A small C++ utility for taking successive PNG animation files and tiling
// them into a single power-of-two PNG

#include "util/directories.h"			// for directory scanning and creation
#include "util/image.h"					// simple image manipulation
#include "util/progressbar.h"			// useful ASCII progress bar indicator

#include "lodepng/lodepng.h"            // for PNG reading and writing

#include <cmath>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

// boring versioning info that actually isn't that helpful
#define VERSION_MAJOR 0
#define VERSION_MINOR 1

// image/file handling
void buildInputImages(const string &inputPrefix, int start, int end, vector<Image*> &inputImages);
void buildTiledImage(vector<Image*> &inputImages, const string &outputFile);

// misc/help/logging
bool isPowerOfTwo(int n);
void printHelpAndUsage();
void log(const string &msg);
void error(const string &msg);

int main(int args, char *argv[])
{
    const int NUM_EXPECTED_ARGS = 5;

    string inputPrefix;                     // starting prefix of files we want to read
    int startIndex;                         // index of first file we read
    int endIndex;                           // index of last file we read
    string outputFile;                      // name of final result to output

    vector<Image*> inputImages;             // list of input images we've read

    // make sure we got the expected number of args
    if(args == NUM_EXPECTED_ARGS)
    {
        // intro message
        cout << "PNGTiler v" << VERSION_MAJOR << "." << VERSION_MINOR << endl;

        // read the args
        inputPrefix = argv[1];
        startIndex = atoi(argv[2]);
        endIndex = atoi(argv[3]);
        outputFile = argv[4];

        // make sure args are sane
        if(startIndex < endIndex)
        {
            // build the list of images
            buildInputImages(inputPrefix, startIndex, endIndex, inputImages);

            // now output them to a single tiled image
            buildTiledImage(inputImages, outputFile);

            // notify the user no further processing is required
            log("done");
        }
        else
        {
            error("starting index must be strictly less than the ending index");
        }
    }
    else
    {
        printHelpAndUsage();
    }

    // exit successfully
    return 0;
}

// image/file handling

void buildInputImages(const string &inputPrefix, int start, int end, vector<Image*> &inputImages)
{
    const int NUM_IMAGES = end - start + 1;

    stringstream filename;
    stringstream output;
    unsigned char *pixels = NULL;

    unsigned int width = 0, height = 0;
    unsigned int expectedWidth = 0, expectedHeight = 0;

    int i;

    for(i = start; i <= end; ++ i)
    {
        // show progress
        printProgressBar("LOADING:", ((i - start) * 100) / NUM_IMAGES);

        // build the filename
        filename.str("");
        filename << inputPrefix << i << ".png";

        // load the file with lodepng
        lodepng_decode32_file(&pixels, &width, &height, filename.str().c_str());

        // make sure we actually loaded the data!
        if(pixels)
        {
            // if this is our first image, record the width and height of what we should expect from now on
            if(i == start)
            {
                // make sure the input size is a perfect square---this will be the standard for all PNGs we loaded here
                if(width != height || !isPowerOfTwo(width))
                {
                    output.str("");
                    output << "the file " << filename.str() << " has a non-power-of-two size " << width << "x" << height;
                    cout << endl;
                    error(output.str());
                }

                // save established image dimensions
                expectedWidth = width;
                expectedHeight = height;
            }
            else
            {
                // otherwise, make sure that the widths and heights match our expectations
                if(width != expectedWidth || height != expectedHeight)
                {
                    output.str("");
                    output << "the first image loaded had size " << expectedWidth << "x" << expectedHeight << " ";
                    output << "but the image " << filename.str() << " has different size " << width << "x" << height;
                    cout << endl;
                    error(output.str());
                }
            }
        }
        else
        {
            cout << endl;
            error("could not load the file " + filename.str());
        }

        // add the image to our image list
        inputImages.push_back(new Image(width, height, pixels));
        free(pixels);
    }

    // progress bar completes
    printProgressBar("LOADING:", 100);
    cout << endl;
}

void buildTiledImage(vector<Image*> &inputImages, const string &outputFile)
{
    const int NUM_IMAGES = inputImages.size();
    const int TILE_WIDTH = inputImages[0] -> getWidth();

    // compute final image width in tiles
    int squareTileCount = ceil(sqrt(NUM_IMAGES));
    int squareWidth = squareTileCount * inputImages[0] -> getWidth();
    int tilesPerRow;
    int finalSize = 2;

    Image *result;
    stringstream ss;
    unsigned int errorCode;
    int x, y;
    int i;

    // compute final image width in pixels and the tile arrangement
    while(finalSize < squareWidth) finalSize = finalSize * 2;
    result = new Image(finalSize);
    tilesPerRow = finalSize / TILE_WIDTH;

    // inform the user of the size of the final image
    ss.str("");
    ss << "final image will be " << finalSize << "x" << finalSize << " ";
    ss << "with tile arrangement " << tilesPerRow << "x" << tilesPerRow;
    log(ss.str());

    // tile each image onto the result
    for(i = 0; i < NUM_IMAGES; ++ i)
    {
        // inform user of progress
        printProgressBar("TILING :", (i * 100) / NUM_IMAGES);

        // figure out where to superimpose the tile onto the result
        x = (i % tilesPerRow) * TILE_WIDTH;
        y = (i / tilesPerRow) * TILE_WIDTH;

        // overwrite that portion of the image
        result -> write(x, y, inputImages[i]);
    }

    // inform user we're done
    printProgressBar("TILING :", 100);
    cout << endl;

    // save the resulting PNG file and check for errors
    log("encoding PNG data to \"" + outputFile + "\"...");
    errorCode = lodepng_encode32_file(outputFile.c_str(),
                                      result -> getData(),
                                      result -> getWidth(),
                                      result -> getHeight());
    if(errorCode)
    {
        ss.str("");
        ss << "could not encode PNG file \"" << outputFile << "\"" << endl;
        ss << "lodePNG error: " << lodepng_error_text(errorCode);
        error(ss.str());
    }

    // free image memory---we're done!
    delete result;
}

// misc/help/logging

// thanks, StackOverflow!
// https://stackoverflow.com/a/108360
bool isPowerOfTwo(int n)
{
    return n != 0 && (n & (n - 1)) == 0;
}

void printHelpAndUsage()
{
	cout << "PNGTiler is an automated tool for producing single power-of-two" << endl;
    cout << "images, constructed by tiling successive onto a single image" << endl;
	cout << "--------------------------------------------------------------------------------" << endl << endl;

	cout << "usage: pngtiler inputPrefix startIndex endIndex outputFile" << endl;
	cout << "\tinputPrefix: prefix used by the sequence of PNG images" << endl;
	cout << "\tstartIndex: first index of PNG images to read" << endl;
	cout << "\tendIndex: last index of PNG images to read" << endl;
	cout << "\toutputFile: resulting PNG file goes here (you should include the extension)" << endl << endl;

	cout << "\ta good example to get things started might be:" << endl;
	cout << "\t\tpngtiler explode 1 50 explode_tiles.png" << endl << endl;
}

void log(const string &msg)
{
	cout << "-- " << msg << endl;
}

void error(const string &msg)
{
	log(msg);
	log("this is a fatal error; PNGTiler will quit immediately");
	exit(1);
}
