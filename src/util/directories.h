#pragma once

#include <string>

bool dirDoesDirectoryExist(const std::string &dir);
bool dirMakeDirectory(const std::string &dir);
std::string dirGetHomeDirectory();

char dirGetDirSep();
