#include "util/image.h"

#include <stdint.h>
#include <cstring>
using namespace std;

Image::Image(unsigned int size)
{
	width = size;
	height = size;
	pixels = new uint8_t[width * height * 4];
	memset(pixels, 0, width * height * 4);
}

Image::Image(unsigned int width, unsigned int height)
{
	this -> width = width;
	this -> height = height;
	pixels = new uint8_t[width * height * 4];
	memset(pixels, 0, width * height * 4);
}

Image::Image(unsigned int width, unsigned int height, uint8_t *data)
{
	this -> width = width;
	this -> height = height;
	pixels = new uint8_t[width * height * 4];
	memcpy(pixels, data, width * height * 4);
}

Image::~Image()
{
	delete[] pixels;
}

unsigned int Image::getWidth() { return width; }
unsigned int Image::getHeight() { return height; }
uint8_t *Image::getData() { return pixels; }

void Image::write(unsigned int x, unsigned int y, Image *img)
{
	unsigned int j;

	for(j = 0; j < img -> getHeight(); ++ j)
	{
		memcpy(&pixels[((y + j) * width * 4) + (x * 4)],
			   &img -> pixels[j * img -> width * 4],
			   sizeof(uint8_t) * img -> width * 4);
	}
}
