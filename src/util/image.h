#pragma once

#include <stdint.h>

class Image
{
private:
	unsigned int width;
	unsigned int height;
	uint8_t *pixels;

public:
	Image(unsigned int size);
	Image(unsigned int width, unsigned int height);
	Image(unsigned int width, unsigned int height, uint8_t *data);
	~Image();

	unsigned int getWidth();
	unsigned int getHeight();
	uint8_t *getData();

	// overwrite a segment of the image
	void write(unsigned int x, unsigned int y, Image *image);
};
