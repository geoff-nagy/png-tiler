#include "util/progressbar.h"

#include <cstdio>
#include <string>
#include <iostream>
using namespace std;

void printProgressBar(int percent)
{
	printProgressBar("", percent);
}

void printProgressBar(const string &msg, int percent)
{
	int i;
	int halfPercent = percent / 2;

	cout << "\r   " << msg << " [";
	for(i = 0; i <= halfPercent; i ++)
		cout << "=";

	if(halfPercent < 50)
		cout << ">";

	for(; i < 50; i ++)
		cout << " ";
	cout << "]: " << percent << "%\r";//%d%%\r", percent);
	fflush(stdout);						// print immediately
}
