#pragma once

#include <string>

void printProgressBar(int percent);
void printProgressBar(const std::string &msg, int percent);
